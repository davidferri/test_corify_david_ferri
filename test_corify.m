n_ecg = matfile('ecgConditioningExample.mat').ecg
[n_muestras , n_deriv] = size(n_ecg)   %cogemos el nº de muestras que tiene cada derivación y el nº total de derivaciones
%n_muestras= 10000  %cogemos las 10000 primeras muestras (los primeros 10 segundos)

fs = matfile('ecgConditioningExample.mat').fs(1,1)  %cogemos la frec de muestreo
%fs=1/fs


%CREAMOS LOS FILTROS QUE NECESITAMOS (si queremos representarlos, esta el
%codigo en modo texto debajo de cada filtro)
%Vamos a utilizar un filtro notch en 50 Hz, dos paso bajo a 40 hz, un cheby de
%tipo 2 y un butterwoth, los 2 de orden 6, y un paso alto a 1 hz butterwoth
%de orden 6

%NOTCH PARA CORTAR EN 50 hz
frec_corte = 50
Q = 1
wo = frec_corte/(fs/2);  %cortamos en 50
bw = wo/Q;  %factor de calidad de 1
[b_50,a_50] = iirnotch(wo,bw)

%figure(1)
%[h,w] = freqz(b_50,a_50,n_muestras,fs);
%plot(w,mag2db(abs(h)))  %en decibelios
%xlabel('Frecuencia (Hz)') 
%ylabel('Magnitud (dB)')


% FILTROS PASO BAJO FREC CORTE 40 HZ
fc_cheby2 = 40
fc_butter = 40
[b_butter,a_butter] = butter(6,fc_butter/(fs/2),"low")  %FILTRO BUTTER DE ORDEN 6 para eliminar altas frecuencias
[b_cheby2,a_cheby2] = cheby2(6,55,fc_cheby2/(fs/2),"low")  %FILTRO CHEBYSEV TIPO 2 ORDEN 6 para tener mas pendiente
%figure(3)
%[h,w] = freqz(b_b,a_b,n_muestras,fs);
%plot(w,mag2db(abs(h)))  %en decibelios
%xlabel('Frecuencia (Hz)') 
%ylabel('Magnitud (dB)')

% FILTRO PASO ALTO FREC CORTE 1 HZ
fc=1
[b_a,a_a] = butter(6,fc/(fs/2),"high")  %orden 6
%figure(4)
%[h,w] = freqz(b_a,a_a,n_muestras,fs);
%plot(w,mag2db(abs(h)))  %en decibelios
%xlabel('Frecuencia (Hz)') 
%ylabel('Magnitud (dB)')


%ANALIZAMOS LAS SEÑALES 3 Y 6, QUE NECESITAN DE CALIBRACIÓN
r = 1 %para representar las figuras en orden
for i=[3 6]
    %CARGAMOS LA SEÑAL
    ecg = matfile('ecgConditioningExample.mat').ecg(1:n_muestras,i)/n_muestras   %la escalamos dividiendo entre el nº de muestras de la señal
    
    
    %REPRESENTAMOS EL ECG QUE NOS DAN
    figure(r)
    t = 0 : 1/fs : (n_muestras-1)/fs
    subplot(4,2,[1 2]), plot(t,ecg)
    grid
    xlabel('t(s)')
    ylabel('V')
    %xlim([0 (n_muestras-1)/fs])
    title(["Señal inicial", i])
    
    
    %SACAMOS EL ECG ORIGINAL EN DBV Y LO REPRESENTAMOS
    t = 0 : 1/fs : (n_muestras-1)/fs   %para representar el ecg en tiempo
    V = mag2db(abs(ecg))              %para representarlo en dBV, V=10^(dBV/20)
    
    subplot(4,2,3), plot(t,V/n_muestras)   %dividimos entre el nº de muestras para reescalar
    grid
    xlabel('Hz')
    ylabel('dBV')
    xlim([0 (n_muestras-1)/fs])
    title(["ECG de la señal",i,"calibrada sin filtrar."])
            
           
    
    %CALCULAMOS LA DFT DEL ECG Y LO REPRESENTAMOS
    f=(-fs/2):(fs/n_muestras):(fs/2);  %definimos los intervalos de frecuencia
    f=f(1:end-1);
    Xk=fft(V,n_muestras)
    Ts=1/fs
    X_m=Ts*fftshift(Xk)  %para centrar el componente de frecuencia 0 
    
    dB = mag2db(abs(X_m))   %para representarlo en dB
    subplot(4,2,4), plot(f,dB) 
    grid
    xlabel('w (Hz)')
    ylabel('dB')
    xlim([0 fs/2])
    title(["DFT de la señal",i," sin filtrar."])
    
    
    %FILTRAMOS
    %filtramos con filtro de paso bajo 
    ecg_filtered = filter(b_cheby2,a_cheby2,V)
    ecg_filtered = filter(b_butter,a_butter,ecg_filtered)
    
    %filtramos con paso alto
    ecg_filtered = filter(b_a,a_a,ecg_filtered)
    
    %filtramos con los filtros notch
    ecg_filtered=filter(b_50,a_50,ecg_filtered)

     %corregimos la desviación, de orden 6
    ecg_filtered = detrend(ecg_filtered,6) 

    

   

    
    %CALCULAMOS LA DFT DE LA SEÑAL FILTRADA Y LA REPRESENTAMOS
    Xk_filtered=fft(ecg_filtered,n_muestras)
    Ts=1/fs
    X_m_filtered=Ts*fftshift(Xk_filtered)  %para centrar el componente de frecuencia 0 
    
    dB_filtered = mag2db(abs(X_m_filtered))   %para representarlo en dB
         
    subplot(4,2,[5 6]), plot(f,dB_filtered) 
    grid
    xlabel('w (Hz)')
    ylabel('dB')
    xlim([0 fs/2])
    ylim([-100 10])
    title(["DFT de la señal",i," filtrada."])
    
    
    %REPRESENTAMOS
    %para quitar parcialmente los artefactos grandes
    ecg_filtered(ecg_filtered>2.5)=0    
    ecg_filtered(ecg_filtered<-2.5)=0
    ecg_filtered=fillmissing(ecg_filtered,'linear')

    subplot(4,2,[7 8]), plot(t,ecg_filtered) 
    grid
    xlabel('Hz')
    ylabel('dBV')
    xlim([0 (n_muestras-1)/fs])
    title(["ECG de la señal",i," filtrado"])

    r = r+1 
end


%AHORA ANALIZAMOS LAS SEÑALES 2, 4 Y 5, QUE ESTAN EN V
r = 3 %para representar las figuras en orden
for i=[2 4 5]
    %CARGAMOS LA SEÑAL
    ecg = matfile('ecgConditioningExample.mat').ecg(1:n_muestras,i)/n_muestras   %la escalamos dividiendo entre el nº de muestras de la señal
    null = find(ecg)  %para comprobar si una derivacion es nula
    
    if isempty(null) == true
        figure(r+10)
        title(["No hay informacion para la señal", i,"(es una señal vacia)"])
        %fprintf("No hay informacion para la señal %d",i)
    
    else
        %REPRESENTAMOS EL ECG QUE NOS DAN Y LO REPRESENTAMOS
        figure(r)
        t = 0 : 1/fs : (n_muestras-1)/fs   %para representar el ecg en tiempo
    
        subplot(4,2,[1 2]), plot(t,ecg)
        grid
        xlabel('t(s)')
        ylabel('V')
        xlim([0 (n_muestras-1)/fs])
        title(["Señal inicial", i," sin filtrar."])
        
        %CALCULAMOS LA DFT DEL ECG Y LO REPRESENTAMOS
        f=(-fs/2):(fs/n_muestras):(fs/2);  %definimos los intervalos de frecuencia
        f=f(1:end-1);
        Xk=fft(ecg,n_muestras)
        Ts=1/fs
        X_m=Ts*fftshift(Xk)  %para centrar el componente de frecuencia 0 
        
        dB = mag2db(abs(X_m))   %para representarlo en dB
        subplot(4,2,[3 4]), plot(f,dB) 
        grid
        xlabel('w (Hz)')
        ylabel('dB')
        xlim([0 fs/2])
        title(["DFT de la señal",i," sin filtrar."])
        
        
        %FILTRAMOS
        %filtramos con filtro de paso bajo 
        ecg_filtered = filter(b_cheby2,a_cheby2,ecg)
        ecg_filtered = filter(b_butter,a_butter,ecg_filtered)
        
        %filtramos con paso alto
        ecg_filtered = filter(b_a,a_a,ecg_filtered)
        
        %filtramos con los filtros notch
        ecg_filtered=filter(b_50,a_50,ecg_filtered)

        %corregimos la desviacion, de orden 6
        ecg_filtered = detrend(ecg_filtered,6)
        
        
        %CALCULAMOS LA DFT DE LA SEÑAL FILTRADA Y LA REPRESENTAMOS
        Xk_filtered=fft(ecg_filtered,n_muestras)
        Ts=1/fs
        X_m_filtered=Ts*fftshift(Xk_filtered)  %para centrar el componente de frecuencia 0 
        
        dB_filtered = mag2db(abs(X_m_filtered))   %para representarlo en dB
             
        subplot(4,2,[5 6]), plot(f,dB_filtered) 
        grid
        xlabel('w (Hz)')
        ylabel('dB')
        xlim([0 fs/2])
        ylim([-100 10])
        title(["DFT de la señal",i," filtrada."])
        
        
        %CALCULAMOS EL ECG FILTRADO Y LO REPRESENTAMOS     
        subplot(4,2,[7 8]), plot(t,ecg_filtered) 
        grid
        xlabel('t (seg)')
        ylabel('V')
        xlim([0 (n_muestras-1)/fs])
        title(["ECG de la señal",i," filtrado"])
    
        r = r+1 
    end
end



%ANALIZAR LA SEÑAL 1, QUE ESTÁ EN dBV
%CARGAMOS LA SEÑAL
ecg = matfile('ecgConditioningExample.mat').ecg(1:n_muestras,1)/n_muestras   %la escalamos dividiendo entre el nº de muestras de la señal
t = 0 : 1/fs : (n_muestras-1)/fs

%REPRESENTAMOS EL ECG QUE NOS DAN
figure(8)
subplot(4,2,[1 2]), plot(t,ecg)
grid
xlabel('Hz')
ylabel('dBV')
xlim([0 (n_muestras-1)/fs])
title(["Señal inicial 1"])


%SACAMOS EL ECG ORIGINAL EN Voltios Y LO REPRESENTAMOS
t = 0 : 1/fs : (n_muestras-1)/fs
ecg = db2mag(ecg)  %db = 20log(V)
subplot(4,2,3), plot(t,ecg)
grid
xlabel('t(s)')
ylabel('V')
xlim([0 (n_muestras-1)/fs])
title(["Señal inicial 1 calibrada"])
     
    
%CALCULAMOS LA DFT DEL ECG Y LO REPRESENTAMOS
f=(-fs/2):(fs/n_muestras):(fs/2);  %definimos los intervalos de frecuencia
f=f(1:end-1);
Xk=fft(ecg,n_muestras)
Ts=1/fs
X_m=Ts*fftshift(Xk)  %para centrar el componente de frecuencia 0 
    
dB = mag2db(abs(X_m))   %para representarlo en dB
subplot(4,2,4), plot(f,dB) 
grid
xlabel('w (Hz)')
ylabel('dB')
xlim([0 fs/2])
title(["DFT de la señal 1 sin filtrar."])
    
    
%FILTRAMOS
%filtramos con filtro de paso bajo 
%ecg_filtered = filter(b_cheby2,a_cheby2,ecg)
ecg_filtered = filter(b_butter,a_butter,ecg_filtered)
    
%filtramos con paso alto
ecg_filtered = filter(b_a,a_a,ecg_filtered)
    
%filtramos con los filtros notch
ecg_filtered=filter(b_50,a_50,ecg_filtered)

%corregimos la desviación, de orden 6
ecg_filtered = detrend(ecg_filtered,6) 
 

%CALCULAMOS LA DFT DE LA SEÑAL FILTRADA Y LA REPRESENTAMOS
Xk_filtered=fft(ecg_filtered,n_muestras)
Ts=1/fs
X_m_filtered=Ts*fftshift(Xk_filtered)  %para centrar el componente de frecuencia 0 
    
dB_filtered = mag2db(abs(X_m_filtered))   %para representarlo en dB
         
subplot(4,2,[5 6]), plot(f,dB_filtered) 
grid
xlabel('w (Hz)')
ylabel('dB')
xlim([0 fs/2])
ylim([-100 10])
title(["DFT de la señal 1 filtrada."])
    
    
%CALCULAMOS EL ECG FILTRADO Y LO REPRESENTAMOS
subplot(4,2,[7 8]), plot(t,ecg_filtered) 
grid
xlabel('t(s)')
ylabel('V')
xlim([0 (n_muestras-1)/fs])
title(["ECG de la señal 1 filtrado"])
